<?php
/*
 * Plugin Name: Custom Login Styles
 * Description: Allows custom login styles from WordPress customizer settings
 * Author: Raquelle Fahle
 * Version: 1.0
 */

require_once(dirname(__FILE__) . '/includes/customizer.php');
require_once(dirname(__FILE__) . '/includes/style.php');

define('RF_CUSTOM_LOGIN_PATH', plugin_dir_path(__FILE__));
define('RF_CUSTOM_LOGIN_FOLDER', __FILE__);
