<?php
function rf_login_styles_customize($wp_customize)
{
    /**
     * Customizer panel
     */
    $wp_customize->add_panel(
        'rf_login_styles_panel',
        [
            'priority'       => 30,
            'capability'     => 'edit_theme_options',
            'title'          => __('Custom Login Styles', 'rf-custom-login'),
            'description'    => __('Allows customization of the WordPress login screen', 'rf-custom-login'),
        ]
    );

    /**
     * Logo section
     */
    $wp_customize->add_section(
        'rf_login_logo_section',
        [
            'priority'  => 5,
            'title'     => __('rf_login Logo', 'rf-custom-login'),
            'panel'     => 'rf_login_styles_panel',
        ]
    );
    $wp_customize->add_setting(
        'rf_login_logo',
        [
            'type'        => 'option',
            'capability'  => 'edit_theme_options',
        ]
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'rf_login_logo',
            [
                'label'    => __('rf_login Logo Image', 'rf-custom-login'),
                'section'  => 'rf_login_logo_section',
                'priority' => 5,
                'settings' => 'rf_login_logo',
            ]
        )
    );

    /**
     * Background section
     */
    $wp_customize->add_section(
        'rf_login_background_section',
        [
            'priority'  => 10,
            'title'     => __('rf_login Background', 'rf-custom-login'),
            'panel'     => 'rf_login_styles_panel',
        ]
    );
    $wp_customize->add_setting(
        'rf_login_background',
        [
            'type'        => 'option',
            'capability'  => 'edit_theme_options',
        ]
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'rf_login_background',
            [
                'label'     => __('rf_login Background Image', 'rf-custom-login'),
                'section'   => 'rf_login_background_section',
                'priority'  => 5,
                'settings'  => 'rf_login_background',
            ]
        )
    );
    $wp_customize->add_setting(
        'rf_login_overlay_color',
        [
            'type'        => 'option',
            'capability'  => 'edit_theme_options',
        ]
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'rf_login_overlay_color',
            [
                'label'       => __('Background Overlay Color', 'rf-custom-login'),
                'section'     => 'rf_login_background_section',
                'priority'    => 15,
                'settings'    => 'rf_login_overlay_color',
                'default'     => '#303030',
                'description' => __('Add an optional overlay color to the background image', 'rf-custom-login')
            ]
        )
    );

    /**
     * Login theme colors
     */
    $wp_customize->add_section(
        'rf_login_color_section',
        [
            'priority'  => 15,
            'title'     => __('Theme Colors', 'rf-custom-login'),
            'panel'     => 'rf_login_styles_panel',
        ]
    );
    $wp_customize->add_setting(
        'rf_login_primary_color',
        [
            'default'    => '#303030',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        ]
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'rf_login_primary_color',
            [
                'label'       => __('Primary Color', 'rf-custom-login'),
                'section'     => 'rf_login_color_section',
                'priority'    => 5,
                'settings'    => 'rf_login_primary_color',
                'description' => __('Form background color', 'rf-custom-login')
            ]
        )
    );
    $wp_customize->add_setting(
        'rf_login_secondary_color',
        [
            'default'    => '#df2b26',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        ]
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'rf_login_secondary_color',
            [
                'label'       => __('Secondary Color', 'rf-custom-login'),
                'section'     => 'rf_login_color_section',
                'priority'    => 10,
                'settings'    => 'rf_login_secondary_color',
                'description' => __('Form button color', 'rf-custom-login')
            ]
        )
    );
    $wp_customize->add_setting(
        'rf_login_tertiary_color',
        [
            'default'    => '#54cdcd',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        ]
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'rf_login_tertiary_color',
            [
                'label'       => __('Tertiary Color', 'rf-custom-login'),
                'section'     => 'rf_login_color_section',
                'priority'    => 15,
                'settings'    => 'rf_login_tertiary_color',
                'description' => __('Form button hover and focus color', 'rf-custom-login')
            ]
        )
    );
    $wp_customize->add_setting(
        'rf_login_alternate_color',
        [
            'default'    => '#ffffff',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        ]
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'rf_login_alternate_color',
            [
                'label'       => __('Alternate Color 1', 'rf-custom-login'),
                'section'     => 'rf_login_color_section',
                'priority'    => 20,
                'settings'    => 'rf_login_alternate_color',
                'description' => __('Form text color', 'rf-custom-login')
            ]
        )
    );
    $wp_customize->add_setting(
        'rf_login_alternate_secondary_color',
        [
            'default'    => '#ffffff',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        ]
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'rf_login_alternate_secondary_color',
            [
                'label'       => __('Alternate Color 2', 'rf-custom-login'),
                'section'     => 'rf_login_color_section',
                'priority'    => 25,
                'settings'    => 'rf_login_alternate_secondary_color',
                'description' => __('Text color below form', 'rf-custom-login')
            ]
        )
    );
}
add_action('customize_register', 'rf_login_styles_customize');
