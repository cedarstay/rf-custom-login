<?php
function rf_login_styles_output()
{
    if ($GLOBALS['pagenow'] !== 'wp-login.php') {
        return;
    }

    $assets_url = plugins_url('/assets', RF_CUSTOM_LOGIN_FOLDER);

    /**
     * Style variables
     */
    $login_logo       = !empty(get_option('login_logo')) ? get_option('login_logo') : $assets_url.'/images/10up-logo-full.svg';
    $login_bg_img     = !empty(get_option('login_background')) ? get_option('login_background') : $assets_url.'/images/default.jpg';
    $login_overlay    = !empty(get_option('login_overlay_color')) ? get_option('login_overlay_color') : '#54cdcd';
    $login_primary    = !empty(get_option('login_primary_color')) ? get_option('login_primary_color') : '#303030';
    $login_secondary  = !empty(get_option('login_secondary_color')) ? get_option('login_secondary_color') : '#df2b26';
    $login_tertiary   = !empty(get_option('login_tertiary_color')) ? get_option('login_tertiary_color') : '#54cdcd';
    $login_alt_1      = !empty(get_option('login_alternate_color')) ? get_option('login_alternate_color') : '#ffffff';
    $login_alt_2      = !empty(get_option('login_alternate_secondary_color')) ? get_option('login_alternate_secondary_color') : '#ffffff';
?>
  <style>
    body.login {
      background: url(<?= $login_bg_img; ?>);
      background-repeat: repeat-y;
      background-size: cover;
    }
    body.login::after {
      content: '';
      display: block;
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: <?= $login_overlay; ?>;
      opacity: 0.7;
      z-index: -1;
    }
    #login h1 {
      overflow: hidden;
      padding-top: 2rem;
    }
    #login h1 a {
      animation: bounceInUp .5s .75s forwards;
      background-image: url(<?= $login_logo; ?>);
      background-size: contain;
      max-width: 200px;
      height: 100px;
      opacity: 0;
    }
    .login form[name="loginform"] {
      animation: slideInLeft .6s forwards;
      background: <?= $login_primary; ?>;
      opacity: 0;
    }
    body.login #nav a,
    body.login #backtoblog a {
      color: <?= $login_alt_2; ?>;
    }
    .login input[name="wp-submit"] {
      background-color: <?= $login_secondary; ?>;
      border-color: <?= $login_secondary; ?>;
      border-radius: 0;
      box-shadow: none;
      text-shadow: none;
    }
    .login input[name="wp-submit"]:hover,
    .login input[name="wp-submit"]:focus {
      background-color: <?= $login_tertiary; ?>;
      border-color: <?= $login_tertiary; ?>;
    }
    .login input[type="text"]:focus,
    .login input[type="password"]:focus,
    .login input[type="checkbox"]:focus {
      outline: none;
      border: 2px solid <?= $login_tertiary; ?>
    }
    .login form label {
      color: <?= $login_alt_1; ?>;
    }
  </style>
    <?php
}
add_action('login_enqueue_scripts', 'rf_login_styles_output');

function rf_login_styles_enqueue()
{
    wp_enqueue_style('rf-login-styles-animations', plugins_url('/assets/styles/animations.css', RF_CUSTOM_LOGIN_FOLDER), false);
}
add_action('login_enqueue_scripts', 'rf_login_styles_enqueue');

/*
 * Login page url and title
 */
add_filter('login_headerurl',  function(){
  return home_url();
});

add_filter('login_headertitle',  function () {
  return get_bloginfo('name');
});
